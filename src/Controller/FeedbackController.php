<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Repository\FeedbackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/feedback")
 */
class FeedbackController extends AbstractController
{
    /**
     * @Route("/", name="feedback_index", methods={"GET"})
     * @param FeedbackRepository $feedbackRepository
     * @return Response
     */
    public function index(FeedbackRepository $feedbackRepository): Response
    {
        return $this->render('feedback/index.html.twig', [
            'feedback' => $feedbackRepository->findAll(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="feedback_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Feedback $feedback
     * @return Response
     */
    public function edit(Request $request, Feedback $feedback): Response
    {
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('feedback_index', [
                'id' => $feedback->getId(),
            ]);
        }

        return $this->render('feedback/edit.html.twig', [
            'feedback' => $feedback,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="feedback_delete", methods={"DELETE"})
     * @param Request $request
     * @param Feedback $feedback
     * @return Response
     */
    public function delete(Request $request, Feedback $feedback): Response
    {
        if ($this->isCsrfTokenValid('delete'.$feedback->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($feedback);
            $entityManager->flush();
        }

        return $this->redirectToRoute('feedback_index');
    }

    /**
     * @Route("/statistics", name="feedback_statistics", methods={"GET"})
     * @param FeedbackRepository $feedbackRepository
     * @return Response
     */
    public function statistics(FeedbackRepository $feedbackRepository){

        dump($feedbackRepository->statistics());
        die();

        return $this->render('feedback/statistics.html.twig', [
            'feedbacks' => $feedbackRepository->statistics(),
        ]);
    }
}
