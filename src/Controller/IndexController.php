<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {

        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();


            $phoneNumber = preg_replace('|\D+|', '', $form->get('phoneNumber')->getData());
            $feedback->setPhoneNumber($phoneNumber);
            $feedback->setIp($request->getClientIp());

            $entityManager->persist($feedback);
            $entityManager->flush();

            $this->addFlash('info',
                'Ваше обращение создано, мы с вами свяжемся в ближайшее время');

            return $this->redirectToRoute('home');
        }

        return $this->render('feedback/new.html.twig', [
            'feedback' => $feedback,
            'form' => $form->createView(),
        ]);

    }
}
