<?php

namespace App\DataFixtures;

use App\Entity\Feedback;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FeedbackFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $feedback_1 = new Feedback();
        $feedback_1->setName('Ольга');
        $feedback_1->setSurname('Иванова');
        $feedback_1->setPartronymic('Васильевна');
        $feedback_1->setEmail('olga@i.ua');
        $feedback_1->setPhoneNumber('380888888888');
        $feedback_1->setTheme($this->getReference('theme_1'));
        $feedback_1->setCreatedAt(new \DateTime('2019-01-17 10:33:18'));
        $feedback_1->setIp('127.0.0.1');

        $manager->persist($feedback_1);
        
        
        $feedback_2 = new Feedback();
        $feedback_2->setName('Татьяна');
        $feedback_2->setSurname('Петренко');
        $feedback_2->setPartronymic('Олеговна');
        $feedback_2->setEmail('tany@i.ua');
        $feedback_2->setPhoneNumber('380795154828');
        $feedback_2->setTheme($this->getReference('theme_1'));
        $feedback_2->setCreatedAt(new \DateTime('2019-01-16 16:45:18'));
        $feedback_2->setIp('127.0.0.1');

        $manager->persist($feedback_2);


        $feedback_3 = new Feedback();
        $feedback_3->setName('Лидия');
        $feedback_3->setSurname('Кочеренко');
        $feedback_3->setPartronymic('Анатольевна');
        $feedback_3->setEmail('lidia@i.ua');
        $feedback_3->setPhoneNumber('380863147529');
        $feedback_3->setTheme($this->getReference('theme_2'));
        $feedback_3->setCreatedAt(new \DateTime('2019-01-15 10:09:18'));
        $feedback_3->setIp('127.0.0.1');

        $manager->persist($feedback_3);


        $feedback_4 = new Feedback();
        $feedback_4->setName('Ольга');
        $feedback_4->setSurname('Иванова');
        $feedback_4->setPartronymic('Васильевна');
        $feedback_4->setEmail('olga@i.ua');
        $feedback_4->setPhoneNumber('380888888888');
        $feedback_4->setTheme($this->getReference('theme_2'));
        $feedback_4->setCreatedAt(new \DateTime('2019-01-15 08:15:18'));
        $feedback_4->setIp('127.0.0.1');

        $manager->persist($feedback_4);


        $feedback_5 = new Feedback();
        $feedback_5->setName('Олег');
        $feedback_5->setSurname('Сидоров');
        $feedback_5->setPartronymic('Сергеевич');
        $feedback_5->setEmail('oleg@i.ua');
        $feedback_5->setPhoneNumber('380637851247');
        $feedback_5->setTheme($this->getReference('theme_3'));
        $feedback_5->setCreatedAt(new \DateTime('2019-01-10 11:40:18'));
        $feedback_5->setIp('127.0.0.1');

        $manager->persist($feedback_5);



        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            ThemesFixtures::class,
        );
    }
}
