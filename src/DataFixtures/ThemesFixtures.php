<?php

namespace App\DataFixtures;

use App\Entity\Themes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ThemesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $theme_1 = new Themes();
        $theme_1->setTitle('Поддержка');

        $manager->persist($theme_1);


        $theme_2 = new Themes();
        $theme_2->setTitle('Отдел продаж');

        $manager->persist($theme_2);


        $theme_3 = new Themes();
        $theme_3->setTitle('Бухгалтерия');

        $manager->persist($theme_3);


        $theme_4 = new Themes();
        $theme_4->setTitle('Оптовые клиенты');

        $manager->persist($theme_4);


        $theme_5 = new Themes();
        $theme_5->setTitle('Сотрудничество');

        $manager->persist($theme_5);


        $manager->flush();

        $this->addReference('theme_1', $theme_1);
        $this->addReference('theme_2', $theme_2);
        $this->addReference('theme_3', $theme_3);
        $this->addReference('theme_4', $theme_4);
        $this->addReference('theme_5', $theme_5);
    }


}
